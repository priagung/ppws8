from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from .views import *
from accordion.apps import AccordionConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

class UnitTest(TestCase):
    def testHomepage(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
    
    def testCorrectTemplate(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testHomepageExist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.driver.quit()

    def testMovingFirstAccordionDown(self):
        self.driver.get(self.live_server_url)
        time.sleep(5)

        firstDownButton = self.driver.find_element_by_id('firstDOWN')
        firstDownButton.click()

        checkAccordion = self.driver.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][2]")
        self.assertEqual(checkAccordion.get_attribute("id"),"second")


    def testMovingSecondAccordionUp(self):
        self.driver.get(self.live_server_url)
        time.sleep(5)

        firstDownButton = self.driver.find_element_by_id('fourthUP')
        firstDownButton.click()

        checkAccordion = self.driver.find_element_by_xpath("//div[@id='accordionExample']/div[@class='card'][3]")
        self.assertEqual(checkAccordion.get_attribute("id"),"third")